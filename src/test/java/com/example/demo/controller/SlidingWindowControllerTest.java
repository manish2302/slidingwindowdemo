package com.example.demo.controller;

import com.example.demo.controller.SlidingWindowController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SlidingWindowController.class)
public class SlidingWindowControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldRejectRequestAfterMaxAllowedInSlidingWindow() throws Exception {
        String userId = "1";

        //given
        executeAllowedRequests(userId);

        //then
        //Not allowed
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sliding-window/is-allowed/{id}", userId))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));
    }

    @Test
    public void shouldNotRejectRequestAfterMaxAllowedOutsideSlidingWindow() throws Exception {
        int tenSecondsInMillis = 10000;
        String userId = "2";

        //given
        executeAllowedRequests(userId);
        Thread.sleep(tenSecondsInMillis);

        //then
        //Allowed
        this.mockMvc.perform(MockMvcRequestBuilders.get("/sliding-window/is-allowed/{id}", userId))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    private void executeAllowedRequests(String userId) throws Exception {
        int count = 0;
        int maxAllowedRequest = 3;

        do {
            //Allowed
            this.mockMvc.perform(MockMvcRequestBuilders.get("/sliding-window/is-allowed/{id}", userId))
                    .andExpect(status().isOk())
                    .andExpect(content().string("true"));
            count++;
        } while (count < maxAllowedRequest);
    }

}
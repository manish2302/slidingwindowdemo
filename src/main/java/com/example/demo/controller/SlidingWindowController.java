package com.example.demo.controller;

import com.sun.istack.internal.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

@RestController
@RequestMapping(value = "/sliding-window")
public class SlidingWindowController {

    private static final Logger logger = LoggerFactory.getLogger(SlidingWindowController.class);
    private static final Integer MAX_REQUEST_LIMIT_PER_TEN_SECOND_PER_USER = 3;
    private static final Integer MILLI_SECONDS_PER_TEN_SECONDS = 10000;

    private static ConcurrentHashMap<String, ConcurrentHashMap<Long, LongAdder>> countMap = new ConcurrentHashMap<>();

    @GetMapping("/is-allowed/{id}")
    public Boolean isAllowedInCurrentSlidingWindow(@NotNull @PathVariable("id") String userId) {
        logger.info("Incoming request for user id {} at time {}", userId, LocalDateTime.now());

        boolean isAllowed;
        long timeKey = System.currentTimeMillis() / MILLI_SECONDS_PER_TEN_SECONDS;

        Map<Long, LongAdder> userCountMap = countMap.computeIfAbsent(userId, u -> new ConcurrentHashMap<>());

        LongAdder currentCount = userCountMap.computeIfAbsent(timeKey, k -> new LongAdder());
        currentCount.increment();

        isAllowed = currentCount.longValue() <= MAX_REQUEST_LIMIT_PER_TEN_SECOND_PER_USER;

        if (!isAllowed) {
            logger.info("reached max threshold request count for user {} in current window", userId);
        }

        return isAllowed;
    }

}